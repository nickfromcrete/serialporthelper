﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerialPortHelper.Application
{
    public class SerialComBuffering
    {
        private SerialPort _com = new SerialPort(SerialPort.GetPortNames()[4], 57600, Parity.None, 8, StopBits.One);
        List<byte> bBuffer = new List<byte>();
        string sBuffer = String.Empty;

        public SerialComBuffering()
        {
            _com.DataReceived += new SerialDataReceivedEventHandler(com_DataReceived);
            _com.Open();

            Console.WriteLine("Waiting for incoming data...");
            Console.ReadKey();
        }

        void com_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            // Use either the binary OR the string technique (but not both)

            // Buffer and process binary data
            while (_com.BytesToRead > 0)
                bBuffer.Add((byte)_com.ReadByte());

            ProcessBuffer(bBuffer);

            // Buffer string data
            //sBuffer += _com.ReadExisting();

            //ProcessBuffer(sBuffer);
        }



        private void ProcessBuffer(string sBuffer)
        {
            // Look in the string for useful information
            // then remove the useful data from the buffer
            Console.WriteLine(sBuffer);
        }



        private void ProcessBuffer(List<byte> bBuffer)
        {
            // Look in the byte array for useful information
            // then remove the useful data from the buffer
            foreach (var item in bBuffer)
            {
                Console.WriteLine(item);
            }
        }
    }
}
