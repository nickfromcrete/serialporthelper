﻿using SerialPortHelper.Application;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace SerialPortHelper.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] ports = SerialPort.GetPortNames();
            foreach (var p in ports)
            {
                Console.WriteLine(p);
            }

            ManagementObjectCollection mReturn;
            ManagementObjectSearcher mSearch;
            mSearch = new ManagementObjectSearcher("Select * from Win32_SerialPort");
            mReturn = mSearch.Get();

            foreach (ManagementObject mObj in mReturn)
            {
                Console.WriteLine(mObj["Name"].ToString());
            }

            var buffer = new SerialComBuffering();

            Console.ReadLine();
        }
    }
}
